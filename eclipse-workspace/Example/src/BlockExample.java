
class Bird{
	static{
		System.out.println("Bird static block");
	}
	{
		System.out.println("Bird Init block");
	}
	Bird(){
		System.out.println("Bird Constructor");
	}
	static{
		System.out.println("Bird static block two");
	}
}

class x {
	public void d1() {
		
	}
}

class Y extends x{
	public void d2() {
		
	}
}

class BirdTwo extends Bird{
	static {
		System.out.println("BirdTwo static block");
	}
	BirdTwo(){
		System.out.println("BirdTwo Constructor");
	}
	{
		System.out.println("BirdTwo Init block");
	}
	static {
		System.out.println("BirdTwo static block 2");
	}
}

public class BlockExample extends BirdTwo{
	
	public BlockExample() {
		main("3");
	}
	
	public static void main(String args) {
		System.out.println("3 " + args);
	}
	
	public static void main(String[] args) {
		System.out.println("2 " +args);
		System.out.println("Pre");
		new BirdTwo();
		System.out.println("Post");
		
		x x = new x();
		x x2 = new Y();
		Y y1 = new Y();
		
		// x2.d2();
		
		//(Y)x2.d2();
		
		((Y)x2).d2();
	}
}
