import static java.lang.System.*;

import com.src.twist.Foo;

interface Device{
	public void doIt();
}

class Electronic implements Device{

	@Override
	public void doIt() {
		
	}
}

abstract class Phone1 extends Electronic{

	@Override
	public void doIt() {
		super.doIt();
	}
	
}

abstract class phone2 extends Electronic{
	@Override
	public void doIt() {
		super.doIt();
	}

	public void doIt(int   x) {}
}

class phone3 extends Electronic implements Device{
	public void doStuff() {}

	@Override
	public void doIt() {
		super.doIt();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
}

enum Animals{
	DOG("woof"), CAT("Meow"), FISH("burble");
	
	String sound;
	Animals(String s){
		sound = s;
	}
}


class Rocket{
	private void blastOff() {
		out.println("Blast");
	}
}

class Hobbit{
	int countGold(int x, int y) {return x + y;}
}

interface Gadget{
	void doStuff();
}

abstract class Electronic1{
	void getPower() {
		System.out.println("Plug In");
	}
}

public class TwistExample extends Electronic1 implements Gadget{
	static Animals a;
	public enum Days{MON, TUE, WED};
	static public void main(String[] _A_V) {
		
		new TwistExample().getPower();
		new TwistExample().doStuff();
		out.println(a.DOG.sound +" "+a.FISH.sound);
		
//		int myGold = 7;
		
//		System.out.println(countGold(myGold, 6));
		
		for(Days d : Days.values())
			;
		Days[] d2 = Days.values();
		System.out.println(d2[2]);
		
		Foo foo = new Foo();
		/*System.out.println(" " + foo.a);
		System.out.println(" " + foo.b);*/
		System.out.println(" " + foo.c);
		
		for(int _x=0; _x < 3;_x++);
		/*int #lb = 7;
		long[] x[5];*/
		Boolean []ba[];
		
	}
	@Override
	public void doStuff() {
		System.out.println("Show Table");
	}
}

/*class __{
	static public void main(String[] _A_V) {
		String $ = "";
		for(int x =0;++x < _A_V.length;) {
			$ += _A_V[x];
		}
		out.print($);
	}
}*/